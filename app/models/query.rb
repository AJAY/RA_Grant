class Query
  def generate_inbound_metrics(from_date, to_date)
    sql = %Q{SELECT count(distinct ch.crt_object_id) count,ch.campaign_id,sum(aqrh.wait_time)/1000 wait_time,
          CASE WHEN ch.talk_time > 0 THEN 'answered' ELSE 'other' END  answered_call
          FROM voice_campaign_aq_request_history vcrh,call_history ch, agent_queue_request_history aqrh 
          where date(ch.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
          and vcrh.request_id = aqrh.request_id AND ch.crt_object_id = vcrh.crt_object_id 
          and ch.campaign_id in (1,2,12,13,17,18) and not is_outbound
          and call_end_time-call_originate_time> interval '5 seconds' group by ch.campaign_id,answered_call;}
    res  = $cc.exec(sql)
  end

  def generate_outbound_metrics(from_date, to_date)
    sql = %Q{select count(*) count,campaign_id,sum(ivr_time)/1000 wait_time
          from call_history
          where date(date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
          and campaign_id in (1,2,12,13,17,18) and is_outbound
          group by campaign_id;}
    res  = $cc.exec(sql)
  end

  def generate_agent_metrics(from_date, to_date)
    sql = %Q{select distinct udh.user_id,ch.campaign_id,ch.is_outbound
          from call_history ch, user_disposition_history udh, contact_center_user u
          where date(udh.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
          and udh.call_id = ch.call_id
          and u.user_id = udh.user_id
          and (u.user_type not in ('Executive','Supervisor','Administrator') or u.user_type is null)
          and ch.campaign_id in (1,2,12,13,17,18)
          order by is_outbound;}
    res  = $cc.exec(sql)
  end

  def generate_main_booking_metrics(from_date, to_date)
    res = nil
    sql = %Q{select count(*) count,lead_source,service_city
          from ola_bookings
          where date_entered  between '#{from_date.strftime('%Y-%m-%d')}' - Interval 330 minute 
          and '#{to_date.strftime('%Y-%m-%d')} 23:59:59' - Interval 330 minute 
          and lead_source in ('outgoing_calls','incoming_direct_calls')
          group by service_city,lead_source;}
    begin  
      $client.with do |conn|
       res =  conn.query(sql)
      end
    rescue Exception => e
      p e
    end
    res
  end   

  def generate_booking_metrics(from_date, to_date)
    res = nil
    sql = %Q{SELECT count(*) count,if(lead_source in ('desktop_website','mobile_website'),'website','others') ls,service_city 
          from ola_bookings 
          where date_entered  between '#{from_date.strftime('%Y-%m-%d')}' - Interval 330 minute 
          and '#{to_date.strftime('%Y-%m-%d')} 23:59:59' - Interval 330 minute 
          group by service_city, ls;}
    begin  
      $client.with do |conn|
       res =  conn.query(sql)
      end
    rescue Exception => e
      p e
    end
    res
  end  

  def generate_mmt_metrics(from_date, to_date)
    res = nil
    sql = %Q{SELECT count(*) count,service_city,status,cancel_reason 
          from ola_bookings 
          where date_entered between '#{from_date.strftime('%Y-%m-%d')}'-Interval 330 minute 
          and '#{to_date.strftime('%Y-%m-%d')} 23:59:59' - Interval 330 minute 
          and lead_source = 'makemytrip' group by service_city,status,cancel_reason;}
    begin  
      $client.with do |conn|
       res =  conn.query(sql)
      end
    rescue Exception => e
      p e
    end
    res
  end  

  def generate_ob_booking_metrics(from_date, to_date)
    res = nil
    sql = %Q{SELECT count(*) count,service_city,status 
          from ola_bookings
          where date_entered  between '#{from_date.strftime('%Y-%m-%d')}' - Interval 330 minute 
          and '#{to_date.strftime('%Y-%m-%d')} 23:59:59' - Interval 330 minute 
          and lead_source = 'outgoing_calls'
          group by service_city,status;}
    begin  
      $client.with do |conn|
       res =  conn.query(sql)
      end
    rescue Exception => e
      p e
    end
    res
  end   

  def generate_ob_hourly_agent_metrics(from_date, to_date)
    sql = %Q{SELECT count(*),t1.campaign_id from 
          (select distinct udh.user_id,ch.campaign_id,EXTRACT(hour from ch.date_added) as hour 
          from call_history ch, user_disposition_history udh, contact_center_user u 
          where date(udh.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' 
          and udh.call_id = ch.call_id and u.user_id = udh.user_id 
          and (u.user_type not in ('Executive','Supervisor','Administrator') or u.user_type is null) 
          and ch.campaign_id in (1,2,12,13,17,18) and  udh.user_id NOT IN 
          (select distinct udh.user_id 
          from call_history ch, user_disposition_history udh, contact_center_user u 
          where date(udh.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' 
          and udh.call_id = ch.call_id and u.user_id = udh.user_id and not ch.is_outbound)) 
          as t1 group by t1.campaign_id;}
    res  = $cc.exec(sql)
  end

  def generate_ob_call_bifurcation_metrics(from_date, to_date)
    sql = %Q{SELECT count(*) count,campaign_id,sum(ivr_time)/1000 wait_time,
          CASE WHEN system_disposition in('CONNECTED','CALL_HANGUP','CALL_DROP','AMD') THEN 'answered' WHEN system_disposition in ('ATTEMPT_FAILED','FAILED','NUMBER_FAILURE','NUMBER_TEMP_FAILURE','PROVIDER_TEMP_FAILURE','PROVIDER_FAILURE') THEN 'failed' ELSE 'other' END  answered_call,
          sum(talk_time)/1000 talk_time
          from call_history 
          where date(date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' 
          and campaign_id in (1,2,12,13,17,18) and is_outbound
          group by campaign_id,answered_call;}
    res  = $cc.exec(sql)
  end  

  def generate_ib_calls_for_booking(from_date, to_date)
    sql = %Q{SELECT count(*) count,ch.campaign_id 
          from user_disposition_history udh,call_history ch 
          where udh.disposition_code in ('Converted', 'converted','Enquiry_Charges','charges','offers_discount','service_related',
          'immediate_stock_out', 'immediate','normal_stock_out','normal_stock_out_2','normal', 'location','location_stock_out',
          'not_interested','not_our_product','Dead_Not our product','expensive','already_booked','Callback/On Hold',
          'callback/on_hold','abrupt_disconnection','Abrupt disconnection') 
          and date(udh.date_Added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' and udh.call_id = ch.call_id 
          and ch.campaign_id in (1,2,12,13,17,18) and not is_outbound group by ch.campaign_id;}
    res  = $cc.exec(sql)
  end

  def generate_ib_calls_of_disposition(from_date, to_date)
    sql = %Q{SELECT count(*) count,ch.campaign_id ,
          CASE WHEN udh.disposition_code in('location','ola_cancellation','normal','immediate') THEN 'oos' 
          WHEN udh.disposition_code in ('business_related','offers_discount') THEN 'business' 
          WHEN udh.disposition_code in ('service_related') THEN 'service' 
          WHEN udh.disposition_code in ('cab_or_driver_details','converted','Converted','customer_cancellation','Answered-Already Booked','already_booked','Answered-Converted','cancellation') THEN 'bookings' 
          WHEN udh.disposition_code in ('inquiry','Answered-Not our product-One way trip','Enquiry_Charges','expensive','charges','not_our_product') THEN 'inquiry' 
          ELSE 'other' END  disposition_code
          from user_disposition_history udh,call_history ch 
          where date(udh.date_Added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' and udh.call_id = ch.call_id 
          and ch.campaign_id in (1,2,12,13,17,18) and not is_outbound group by ch.campaign_id,disposition_code;}
    res  = $cc.exec(sql)
  end

  def generate_ib_repeat_callers(from_date,to_date)
    sql = %Q{ SELECT count(distinct a.phone_mobile) count,a.campaign_id 
            FROM (SELECT distinct (substring(phone from (char_length(phone) - 9) for 10)) phone_mobile,ch.campaign_id 
            FROM call_history ch 
            where date(ch.date_added) BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
            AND ch.campaign_id in (1,2,12,13,17,18) and not is_outbound and ch.talk_time > 0
            and call_end_time-call_originate_time> interval '5 seconds' ) a 
            LEFT JOIN (SELECT (substring(phone from (char_length(phone) - 9) for 10)) phone_mobile  
            FROM call_history ch where date(ch.date_added)<'#{from_date.strftime("%Y-%m-%d")}' AND ch.campaign_id in (1,2,12,13,17,18) 
            and not is_outbound and call_end_time-call_originate_time> interval '5 seconds') b 
            ON a.phone_mobile = b.phone_mobile where b.phone_mobile is not null group by a.campaign_id;}
    res  = $cc.exec(sql)       
  end

  def generate_ib_unique_callers(from_date, to_date)
    sql = %Q{SELECT count(distinct (substring(phone from (char_length(phone) - 9) for 10))) count,ch.campaign_id
          FROM voice_campaign_aq_request_history vcrh,call_history ch
          where date(ch.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
          AND ch.crt_object_id = vcrh.crt_object_id 
          and ch.campaign_id in (1,2,12,13,17,18) and not is_outbound and ch.talk_time > 0
          and call_end_time-call_originate_time> interval '5 seconds' group by ch.campaign_id;}
    res  = $cc.exec(sql)
  end

  def generate_ib_stockout(from_date, to_date)
    res = nil
    sql = %Q{SELECT count(*) count,primary_address_city,if(service_type='p2p','p2p','rental') filter_stype,
           if(status in('stock_out','Stock Out'),'stock_out','other') filter_status 
           from leads 
           where date_entered  between '#{from_date.strftime('%Y-%m-%d')}' - Interval 330 minute 
           and '#{to_date.strftime('%Y-%m-%d')} 23:59:59' - Interval 330 minute 
           and lead_source = 'incoming_direct_calls' 
           group by primary_address_city,filter_status,filter_stype;}
     begin  
       $client.with do |conn|
        res =  conn.query(sql)
       end
     rescue Exception => e
       p e
     end
    res
  end

  def generate_ib_converted_calls_for_booking(from_date, to_date)
    res = nil
    sql = %Q{SELECT count(*) count,service_city from ola_bookings 
          where date_entered  between '#{from_date.strftime('%Y-%m-%d')}' - Interval 330 minute 
          and '#{to_date.strftime('%Y-%m-%d')} 23:59:59' - Interval 330 minute 
          and lead_source = 'incoming_direct_calls' 
          group by lead_source,service_city;}
    begin  
      $client.with do |conn|
       res =  conn.query(sql)
      end
    rescue Exception => e
      p e
    end
    res
  end

  def generate_ib_details(from_date, to_date)

    sql = %Q{SELECT count(distinct ch.crt_object_id) count,ch.campaign_id,sum(hold_time)/1000 ht,sum(talk_time)/1000 tt,sum(aqrh.wait_time)/1000 it,
          CASE WHEN system_disposition = 'CONNECTED' THEN 'answered' WHEN system_disposition in ('CALL_HANGUP','CALL_NOT_PICKED')
          THEN 'abandoned' ELSE 'other' END  answered_call,
          CASE WHEN aqrh.wait_time > 20000 THEN 'gt20' ELSE 'lt20' END filter_wait_time
          from call_history ch,voice_campaign_aq_request_history vcrh,agent_queue_request_history aqrh 
          where date(ch.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'and ch.campaign_id in (1,2,12,13,17,18) and not ch.is_outbound 
          and vcrh.request_id = aqrh.request_id AND ch.crt_object_id = vcrh.crt_object_id 
          and ch.call_end_time-ch.call_originate_time> interval '5 seconds'
          group by ch.campaign_id,answered_call,filter_wait_time;}
    res  = $cc.exec(sql)
  end

  def generate_ib_wraptime_details(from_date, to_date)
    sql = %Q{SELECT ch.campaign_id,sum(wrap_time)/1000 wrap_time
          from call_history ch, user_disposition_history udh
          where date(udh.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' and udh.call_id = ch.call_id  and not is_outbound 
          and ch.campaign_id in (1,2,12,13,17,18) group by ch.campaign_id;}
    res  = $cc.exec(sql)
  end

  def generate_ib_hourly_agent_metrics(from_date, to_date)
    sql = %Q{SELECT distinct cctu.contact_center_team_id,ccu.user_id,ush.login_time,ush.logout_time
          from contact_center_team_user cctu,contact_center_user ccu, user_session_history ush
          where cctu.contact_center_team_id in (1,2,4,5,7,8)
          and cctu.contact_center_user_id = ccu.id
          and ush.user_id = ccu.user_id and (date(ush.login_time) BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' or date(ush.logout_time) BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}');}

    res  = $cc.exec(sql)
  end

  def generate_ib_converted_booking_metrics(from_date, to_date)
    sql = %Q{SELECT count(*) count,ch.campaign_id, sum(udh.talk_time)/1000 talk_time,sum(udh.wrap_time)/1000 wrap_time,sum(udh.hold_time)/1000 hold_time 
          from call_history ch, user_disposition_history udh
          where  date(udh.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' and udh.call_id = ch.call_id  and not is_outbound 
          and disposition_code in ('converted', 'Converted')
          and ch.campaign_id in (1,2,12,13,17,18) group by ch.campaign_id;}
    res  = $cc.exec(sql)
  end

  def get_agent_team_details(from_date, to_date)
    sql = %Q{SELECT distinct cctu.contact_center_team_id,ccu.user_id
          from contact_center_team_user cctu,contact_center_user ccu, user_session_history ush
          where cctu.contact_center_team_id in (1,2,4,5,7,8)
          and cctu.contact_center_user_id = ccu.id
          and ush.user_id = ccu.user_id and date(ush.login_time) BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}';}
    res  = $cc.exec(sql)
  end

  def generate_ob_not_our_product(from_date, to_date)
    sql = %Q{SELECT count(*) count,ch.campaign_id, udh.disposition_code
          from call_history ch, user_disposition_history udh
          where  date(udh.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}' and udh.call_id = ch.call_id  and is_outbound 
          and ch.campaign_id in (1,2,12,13,17,18) and udh.disposition_code in ('not_our_product','Answered-Not our product-One way trip') group by ch.campaign_id,udh.disposition_code;}
    res  = $cc.exec(sql)
  end


  def generate_ib_queued(from_date, to_date)
     sql = %Q{SELECT count(*) count,aq.campaign_context_id
            FROM voice_campaign_aq_request_history vcrh, agent_queue_request_history aqrh, agent_queue aq
            WHERE vcrh.request_id = aqrh.request_id AND aq.id = aqrh.agent_queue_id
            and date(aqrh.request_time)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
            and aqrh.wait_time >= 5 and aq.campaign_context_id in (2,12,17) group by aq.campaign_context_id;}
    res  = $cc.exec(sql)
  end

  def generate_ib_abandoned_8seconds(from_date, to_date)
     sql = %Q{SELECT count(*) count,ch.campaign_id
            FROM voice_campaign_aq_request_history vcrh,call_history ch
            WHERE vcrh.crt_object_id = ch.crt_object_id and date(ch.date_added)  BETWEEN '#{from_date.strftime("%Y-%m-%d")}' AND '#{to_date.strftime("%Y-%m-%d")}'
            and ch.campaign_id in (1,2,12,13,17,18) and system_disposition in ('CALL_HANGUP','CALL_NOT_PICKED')
            and ch.call_end_time - ch.call_originate_time < interval '9 seconds' group by ch.campaign_id;}
     res  = $cc.exec(sql)
  end

end
