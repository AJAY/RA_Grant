$(document).ready(function() {
  
  $("#intake_form").validate(
  {         
    // Rules for form validation
    rules:
    {
      sponsor:
      {
        required: true,
        minlength: 2,
        maxlength: 20
      }
      
    },
    
    // Messages for form validation
    messages:
    {
      sponsor:{
        required: 'Please enter sponsor name'
      }
    },          
    
    // Do not change code below
    errorPlacement: function(error, element)
    {
      error.insertAfter(element).before("<br />");
    }
  });


  $("#due_Date").datepicker({
      dateFormat: 'mm-dd-yy',
      minDate: '0',
      onSelect: function(selected) {
        $("#due_date").datepicker("option","minDate", selected);
      }
  });

  $("#startDate").datepicker({
      dateFormat: 'mm-dd-yy',
      maxDate: '0',
      onSelect: function(selected) {
        $("#endDate").datepicker("option","minDate", selected);
      }
  });

  $("#endDate").datepicker({
      dateFormat: 'mm-dd-yy',
      maxDate: '0',
      onSelect: function(selected) {
        $("#startDate").datepicker("option","maxDate", selected);
      }
  })

  $('input[type=radio]').change(function () {
    if (!this.checked) return
    $('.collapse').not($('div.' + $(this).attr('class'))).slideUp();
    $('.collapse.' + $(this).attr('class')).slideDown();
	});

	$(".clear_radio").click(function(){
    $(this).siblings("input[type=radio]").prop('checked', false);
	});

  $(".budget").change(function(){
    if($(this).val()=="yes")
    {
      $("#budget").css({"display":"block", "margin-top":"10px"});
    }
    else
    {
       $("#budget").css("display", "none");
    }
  });

  $(".conflict").change(function(){
    if($(this).val()=="yes")
    {
      $("#conflict").css({"display":"block", "margin-top":"10px"});
    }
    else
    {
       $("#conflict").css("display", "none");
    }
  });


  $(".funding_check").change(function(){
    if($(this).val()=="provide_link")
    {
      $("#funding").css({"display":"block", "margin-top":"10px"});
      $("#file_upload").css("display", "none");
    }
    else if($(this).val()=="attach_file")
    {
      $("#file_upload").css({"display":"block", "margin-top":"10px"});
      $("#funding").css("display", "none");
    }
  });


  $("#add_row").click(function(){
    var row = "<td> CO-PI</td><td><input name='name' type='text' placeholder='Name' class='form-control input-md'  /> </td><td><input  name='mail' type='text' placeholder='Center Name' class='input-medium'  class='form-control input-md'></td><td><input  name='mobile' type='text' class='input-small' placeholder='Allocation %'  class='form-control input-md'></td><td><a class='pull-right btn btn-danger delete_row'>Delete</a></td>";
    $('#tab_logic').append('<tr>'+row+'</tr>');

    $(".delete_row").click(function(){
      $(this).closest ('tr').remove ();
    });
  });
  
  $("#add_clrow").click(function(){

    var row = "<td rowspan='4'><li></li></td> <td>Name</td><td><input name='name' type='text' placeholder='Name' class='form-control input-md'  /> </td><td rowspan='4'>  <a class='pull-right btn btn-danger delete_box'> Delete</a></td></tr><tr> <td> Contact </td> <td> <input type='text' name='contact' placeholder='Contact' class='form-control'/> </td> </tr> <tr> <td> Which Institution is the Lead? </td> <td> <input type='text' name='institution' placeholder='Institution Name' class='form-control'/> </td> </tr> <tr> <td> Name of Lead PI at collaborating institutions? </td> <td> <input type='text' name='pi' placeholder='Name of Lead PI' class='form-control'/> </td> </tr>";
    $('#tab_col').append('<tr>'+row+'</tr>');

    $(".delete_box").click(function(){
      $(this).closest ('tr').next().remove ();
      $(this).closest ('tr').next().remove ();
      $(this).closest ('tr').next().remove ();
      $(this).closest ('tr').remove ();
    });
  });
          
  $(".delete_box").click(function(){
    $(this).closest ('tr').next().remove ();
    $(this).closest ('tr').next().remove ();
    $(this).closest ('tr').next().remove ();
    $(this).closest ('tr').remove ();
  });
       
});
