require 'bundler'
require 'yaml'
require 'mysql2'

module Analytics

  class Application
    def self.root(path = nil)
      @_root ||= File.expand_path(File.dirname(__FILE__))
      path ? File.join(@_root, path.to_s) : @_root
    end

    def self.env
      @_env ||= ENV['RACK_ENV'] || 'development'
    end

    # Initialize the application
    def self.initialize!
    end

  end
end

Bundler.require(:default, Analytics::Application.env)

config = YAML.load(File.read(File.join(Analytics::Application.root, "config", "database.yml")))[Analytics::Application.env]

$client = ConnectionPool.new(:size => 5, :timeout => 5) { Mysql2::Client.new(
                            :host     => config["blackbox_database"]["host"]||"localhost",
                            :database => config["blackbox_database"]["database"],
                            :username => config["blackbox_database"]["username"],
                            :password => config["blackbox_database"]["password"],
                            :reconnect => true, 
                            :wait_timeout => 2147483)}


# Preload application classes
Dir['./app/**/*.rb'].each {|f| require f}
set :views, settings.root + '/app/views'
set :models, settings.root + '/app/models'
$ver_number = Time.now.to_i

